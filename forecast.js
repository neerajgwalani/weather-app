
const key = "becd574c04d6d9e8bdfe57632571f25e";

const getForecast = async(city) => {
    const base = "http://api.openweathermap.org//data/2.5/forecast";
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base+query);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Status code : '+response.status);
    }
    
}